import Searchbar from "@/src/components/ui/Text/Searchbar";
import { Heading, Box, Link as CLink, Text, Divider } from "@chakra-ui/react";
import Head from "next/head";

export default function Search() {
  return (
    <>
      <Head>
        <title>Search</title>
        <meta name="description" content="About Plutus Project-Based Learning" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={{ base: "95%", md: "85%", lg: "75%" }} mx="auto">
        <Heading py="10" size="2xl">
          Search
        </Heading>
        <Divider />
        {/*<Text fontSize="2xl">
        Plutus Project-Based Learningは、Gimbalabsから提供される無料のコースです。Plutus Project-Based Learningの目標は、Cardanoの開発エコシステムにおいて実際のプロジェクトへの貢献者となることをサポートすることです。このコースは自己ペースであり、誰でも自分の都合の良い時間にコースを進めることができます。また、私たちは毎週<CLink href="live-coding">2回のライブコーディングセッション</CLink>を開催しており、学生は質問をするために集まり、一緒に学ぶことができます。
        </Text>
        <Text fontSize="2xl" py="5">
          このコースについて学ぶ最良の方法は、<CLink href="/get-started">始めること</CLink>です。私たちは飛び込んで学習を始め、成長する学習者とビルダーのコミュニティに参加することをお勧めします。
        </Text>
        <Divider />*/}
        <Searchbar />
      </Box>
    </>
  );
}
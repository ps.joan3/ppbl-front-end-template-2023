import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import dynamic from "next/dynamic";

import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json";

const LessonPage = () => {
  const router = useRouter();
  const { module } = router.query;
  let moduleId = module ? parseInt(module?.toString()) : 0;
  const moduleSelected = slt.modules.find((m) => m.number === moduleId);

  const status = null;

  // // the orginal `lessons` array TODO remove
  // Sidebar items are generated from module.lessons i.e. from the JSON file
  // Here we simply set the contents by matching the slug and key
  // const lessons = [
  //   { key:"slts", component:<><SLTs103 /></>},
  //   { key:"1031", component:<Lesson1031 />},
  //   { key:"1032", component:<Lesson1032 />},
  //   { key:"1033", component:<Lesson1033 />},
  //   { key:"summary", component:<Summary103 />},
  //   { key:"commit", component:<Commit103 />},
  // ]

  const [lessons, setLessons] = useState<{}[]>([]);

  useEffect(() => {
    if (moduleSelected) {
      const _lessons = moduleSelected.lessons.map(function (l) {
        const LessonJsx = dynamic(
          () => import(`@/src/components/course-modules/${module}/${l.path}`),
          {
            ssr: false,
          }
        );
        return {
          key: l.slug,
          component: <LessonJsx />,
        };
      });
      setLessons(_lessons);
    }
  }, [module, moduleSelected]);

  return (
    <ModuleLessons
      items={moduleSelected?.lessons ?? []}
      modulePath={`/modules/${module}`}
      selected={0}
      lessons={lessons}
      status={status}
    />
  );
};

export default LessonPage;
import React from 'react';
import { Heading, Box, Link as CLink, Text, Divider } from "@chakra-ui/react";
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, SearchBox, Hits, Highlight, RefinementList, Snippet } from 'react-instantsearch';

import 'instantsearch.css/themes/algolia.css';

import { lightfair } from 'react-syntax-highlighter/dist/esm/styles/hljs';

const searchClient = algoliasearch('SWXCDB662U', '2184434b1e66d8057eb3704be6c6855e');

export default function Searchbar() {
  return (
    <InstantSearch searchClient={searchClient} indexName="test_SEARCH" insights routing={true}>
        <Divider/>
        <Box>            
            <SearchBox classNames={{
                root: 'p-3 shadow-sm',
                form: 'relative',
                input: 'block w-full pl-9 pr-3 py-2 bg-white border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 rounded-md focus:ring-1',
                submitIcon: 'absolute top-0 left-0 bottom-0 w-6',
            }} />
        </Box>
        <Divider/>
        <RefinementList attribute="topic" /> 
        <Box> 
        <Hits hitComponent={Hit} classNames={{
                root: 'p-3 shadow-sm'                       
            }}/>
        </Box>
    </InstantSearch>
  );
}

function Hit({ hit }: any) {
    return (    
        <article>          
            <h1>                        
                <span style={{color: 'white', backgroundColor: 'green'}}><Highlight attribute="topic" hit={hit} /></span>{' '}             
            </h1>
            <h1>
                <span style={{color: 'white', backgroundColor: 'green'}}>{hit.lesson}</span>{' '}
            </h1>            
        </article>
    );
}



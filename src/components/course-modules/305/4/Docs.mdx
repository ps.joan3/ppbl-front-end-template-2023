import Quiz from "@/src/components/lms/Lesson/Quiz.tsx";

## Basic transaction

The most basic transaction is a transfer of some ADA from one account to another.

Go to [Mesh Playground](https://meshjs.dev/apis/transaction) and see how to do that.

<Quiz
  question="How to send some ADA from one account to another?"
  choices={[
    { value: "wrong1", label: "tx.sendAda('<ADDRESS>', '<AMOUNT IN ADA>')" },
    {
      value: "right",
      label: "tx.sendLovelace('<ADDRESS>', '<AMOUNT IN LOVELACE>')",
    },
  ]}
  correctAnswer="right"
  correctNote={<></>}
  wrongNote={
    <>
      Check <a href="https://meshjs.dev/apis/transaction">Mesh documentation</a>{" "}
      on how to create a transaction to send ADA.
    </>
  }
/>

To send ADA from one account to another, you need to create a transaction.

First, we import the `Transaction` class from `@meshsdk/core`, then we create a new transaction with `Transaction`, passing the initiator wallet as an argument.

Then, we call the `sendLovelace` method on the transaction, passing the address of the recipient and the amount in lovelace.

Next, we call the `build` method on the transaction to build the transaction. This will return an unsigned transaction.

To sign the transaction, we call the `signTx` method on the wallet, passing the unsigned transaction as an argument. This will return a signed transaction.

Finally, we call the `submitTx` method on the wallet, passing the signed transaction as an argument. This will return the transaction hash.

```js
import { Transaction } from "@meshsdk/core";

const tx = new Transaction({ initiator: wallet }).sendLovelace(
  "addr_test1vpvx0sacufuypa2k4sngk7q40zc5c4npl337uusdh64kv0c7e4cxr",
  "1000000"
);

const unsignedTx = await tx.build();
const signedTx = await wallet.signTx(unsignedTx);
const txHash = await wallet.submitTx(signedTx);
```

## Locking tokens into a script address

In this section, we will be locking 2 ADA from your wallet to a smart contract address.

We will create a new smart contract using plu-ts. Let's learn how we can do that.

### Create a new contract with plu-ts

We will create a contract that has 2 conditions to be met before the tokens can be redeemed

- a secret message `a super secret key`
- the transaction must be signed by the owner

Create a file call `contract.ts` and add the following code:

```js
import {
  bool,
  compile,
  makeValidator,
  pfn,
  Script,
  PPubKeyHash,
  PScriptContext,
  bs,
} from "@harmoniclabs/plu-ts";

const contract = pfn(
  [PPubKeyHash.type, bs, PScriptContext.type],
  bool
)((owner, message, ctx) => {
  const isBeingPolite = message.eq("a super secret key");

  const signedByOwner = ctx.tx.signatories.some(owner.eqTerm);

  return isBeingPolite.and(signedByOwner);
});

const untypedValidator = makeValidator(contract);
const compiledContract = compile(untypedValidator);

const script = new Script("PlutusScriptV2", compiledContract);

export const scriptCbor = script.cbor.toString();
```

First, we import the necessary modules from plu-ts.

Then, we create a new contract with `pfn`. `pfn` takes two arguments: the first is the types of the arguments of the contract, and the second is the type of the return value of the contract.

The contract takes three arguments: the owner of the contract, the message, and the context. The context contains information about the transaction.

The contract returns a boolean value.

Next, we create an untyped validator with `makeValidator`, passing the contract as an argument, and compile the untyped validator with `compile`.

Finally, we create a new script with the compiled contract, and export the CBOR of the script.

### Create transaction to lock tokens into a script address

Now that we have created a new contract, let's lock some tokens into a script address.

Create a function, `lockAsset()` and add the following code:

```js
async function lockAsset() {
  
  // create PlutusScript from CBOR
  const script: PlutusScript = {
    code: scriptCbor,
    version: "V2",
  };

  // get script address
  const scriptAddress = resolvePlutusScriptAddress(script, 0);

  // get wallet keyhash
  const address = (await wallet.getUsedAddresses())[0];
  const walletKeyhash = resolvePaymentKeyHash(address);

  // create transaction
  const tx = new Transaction({ initiator: wallet }).sendLovelace(
    {
      address: scriptAddress,
      datum: {
        value: walletKeyhash,
        inline: true,
      },
    },
    "2000000"
  );

  const unsignedTx = await tx.build();
  const signedTx = await wallet.signTx(unsignedTx);
  const txHash = await wallet.submitTx(signedTx);

}
```

First, we create a new `PlutusScript` object with the CBOR of the script and the version of the script, and get the script address with `resolvePlutusScriptAddress`, passing the script and the network ID as arguments.

We also get the keyhash of the wallet with `resolvePaymentKeyHash`, passing the address of the connected wallet as an argument. The transaction we are creating will use this keyhash as the datum.

Then, we create a new transaction with `Transaction`, passing the initiator wallet as an argument. We call the `sendLovelace` method on the transaction, passing the address of the recipient and the amount in lovelace. Note that, here you can use `sendAssets()` to send other assets to the script address too.

We call the `build` method on the transaction to build the transaction. This will return an unsigned transaction.

To sign the transaction, we call the `signTx` method on the wallet, passing the unsigned transaction as an argument. This will return a signed transaction.

Finally, we call the `submitTx` method on the wallet, passing the signed transaction as an argument. This will return the transaction hash.

## Redeeming tokens from a script address

Now that we have locked some tokens into a script address, let's redeem them.

Create a function, `redeemAsset()` and add the following code:

```js
async function redeemAsset() {

  // create PlutusScript from CBOR
  const script: PlutusScript = {
    code: scriptCbor,
    version: "V2",
  };

  // get script address
  const scriptAddress = resolvePlutusScriptAddress(script, 0);

  // get wallet keyhash and its data hash
  const address = (await wallet.getUsedAddresses())[0];
  const walletKeyhash = resolvePaymentKeyHash(address);
  const dataHash = resolveDataHash(walletKeyhash);

  // get utxo from script
  const blockchainProvider = new KoiosProvider("preprod");
  const utxos = await blockchainProvider.fetchAddressUTxOs(
    scriptAddress,
    "lovelace"
  );
  let utxo = utxos.find((utxo: any) => {
    return utxo.output.dataHash == dataHash;
  });

  // define redeemer
  const redeemer = {
    data: "a super secret key",
  };

  // create transaction
  const tx = new Transaction({ initiator: wallet })
    .redeemValue({
      value: utxo,
      script: script,
      datum: utxo,
      redeemer: redeemer,
    })
    .sendValue(address, utxo)
    .setRequiredSigners([address]);

  const unsignedTx = await tx.build();
  const signedTx = await wallet.signTx(unsignedTx, true);
  const txHash = await wallet.submitTx(signedTx);

}
```

Ok there is a lot going on, so let's break it down.

Like locking assets, we first create a new `PlutusScript` object with the CBOR of the script and the version of the script, and get the script address with `resolvePlutusScriptAddress`, passing the script and the network ID as arguments.

We also get the keyhash of the wallet with `resolvePaymentKeyHash`, passing the address of the connected wallet as an argument. The transaction we are creating will use this keyhash as the datum.

Then, we get the data hash of the keyhash with `resolveDataHash`, passing the keyhash as an argument. We will use this data hash to get the utxo from the script address.

To get the UTXO from script, we create a new `KoiosProvider` object with the network ID as an argument, and call the `fetchAddressUTxOs` method on the provider, passing the script address and the asset name as arguments. This will return the utxos of the script address.

We filter the set of UTXOS that matches the data hash, to find the UTXO that belongs to the wallet. We will use this UTXO to redeem the tokens, and use it in our transaction.

Next, we define the redeemer. The redeemer is the value that will be passed to the validator. In this case, we are passing a string, `a super secret key`, as this is what was defined in the contract.

Then, we create a new transaction with `Transaction`, passing the initiator wallet as an argument. We call the `redeemValue` method on the transaction, passing the UTXO, the script, the datum, and the redeemer as arguments. We call the `sendValue` method on the transaction, passing the address of the recipient and the amount in lovelace as arguments. We call the `setRequiredSigners` method on the transaction, passing the address of the wallet as an argument.

We call the `build` method on the transaction to build the transaction. This will return an unsigned transaction. The `signTx` method on the wallet, passing the unsigned transaction as an argument. This will return a signed transaction. Finally, we call the `submitTx` method on the wallet, passing the signed transaction as an argument. This will return the transaction hash.

Now that we have created the contract, locked some tokens into the script address, and redeemed them. In the next lesson, we will learn how to create the user interface to interact with these transactions.

import MDXLessonLayout from "@/src/components/lms/Lesson/MDXLessonLayout.tsx";
export default ({ children }) => <MDXLessonLayout>{children}</MDXLessonLayout>;
